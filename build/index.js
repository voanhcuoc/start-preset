'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.prepush = exports.ci = exports.coverage = exports.tdd = exports.test = exports.lint = exports.dev = exports.build = exports.start = undefined;

var _start = require('start');

var _start2 = _interopRequireDefault(_start);

var _startPrettyReporter = require('start-pretty-reporter');

var _startPrettyReporter2 = _interopRequireDefault(_startPrettyReporter);

var _startEnv = require('start-env');

var _startEnv2 = _interopRequireDefault(_startEnv);

var _startFiles = require('start-files');

var _startFiles2 = _interopRequireDefault(_startFiles);

var _startWatch = require('start-watch');

var _startWatch2 = _interopRequireDefault(_startWatch);

var _startClean = require('start-clean');

var _startClean2 = _interopRequireDefault(_startClean);

var _startRead = require('start-read');

var _startRead2 = _interopRequireDefault(_startRead);

var _startBabel = require('start-babel');

var _startBabel2 = _interopRequireDefault(_startBabel);

var _startWrite = require('start-write');

var _startWrite2 = _interopRequireDefault(_startWrite);

var _startEslint = require('start-eslint');

var _startEslint2 = _interopRequireDefault(_startEslint);

var _startTape = require('start-tape');

var _startTape2 = _interopRequireDefault(_startTape);

var _startIstanbul = require('start-istanbul');

var istanbul = _interopRequireWildcard(_startIstanbul);

var _startCodecov = require('start-codecov');

var _startCodecov2 = _interopRequireDefault(_startCodecov);

var _tapSpec = require('tap-spec');

var _tapSpec2 = _interopRequireDefault(_tapSpec);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var start = exports.start = (0, _start2.default)((0, _startPrettyReporter2.default)());

var build = exports.build = function build() {
  return start((0, _startEnv2.default)('NODE_ENV', 'production'), (0, _startFiles2.default)('build/'), (0, _startClean2.default)(), (0, _startFiles2.default)('src/**/*.js'), (0, _startRead2.default)(), (0, _startBabel2.default)(), (0, _startWrite2.default)('build/'));
};

var dev = exports.dev = function dev() {
  return start((0, _startEnv2.default)('NODE_ENV', 'development'), (0, _startFiles2.default)('build/'), (0, _startClean2.default)(), (0, _startFiles2.default)('src/**/*.js'), (0, _startWatch2.default)(function (file) {
    return start((0, _startFiles2.default)(file), (0, _startRead2.default)(), (0, _startBabel2.default)(), (0, _startWrite2.default)('build/'));
  }));
};

var lint = exports.lint = function lint() {
  return start((0, _startEnv2.default)('NODE_ENV', 'test'), (0, _startFiles2.default)(['src/**/*.js', 'test/**/*.js']), (0, _startEslint2.default)());
};

var test = exports.test = function test() {
  return start((0, _startEnv2.default)('NODE_ENV', 'test'), (0, _startFiles2.default)('test/**/*.js'), (0, _startTape2.default)(_tapSpec2.default));
};

var tdd = exports.tdd = function tdd() {
  return start((0, _startFiles2.default)(['src/**/*.js', 'test/**/*.js']), (0, _startWatch2.default)(test));
};

var coverage = exports.coverage = function coverage() {
  return start((0, _startEnv2.default)('NODE_ENV', 'test'), (0, _startFiles2.default)('coverage/'), (0, _startClean2.default)(), (0, _startFiles2.default)('src/**/*.js'), istanbul.instrument({ esModules: true }), test, istanbul.report(['lcovonly', 'html', 'text-summary']));
};

var ci = exports.ci = function ci() {
  return start(lint, coverage, (0, _startFiles2.default)('coverage/lcov.info'), (0, _startRead2.default)(), (0, _startCodecov2.default)());
};

var prepush = exports.prepush = function prepush() {
  return start(lint, coverage);
};